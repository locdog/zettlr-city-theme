# Zettlr-City Theme

This is a modification to the Zettlr custom css theme "joaquina". The Inter font family adds an urban, metropolitan feel to writing. This custom css may be used with all of Zettlr's installed theme colours.

## The Urban Metropolitan Zettlr theme

This custom css is a modification of [the Zettlr custom css theme "joaquina"](https://github.com/ppgfryvo/zettlrtemplate.css).

![](https://gitlab.com/locdog/zettlr-city-theme/-/raw/main/Screenshot-Zettlr-City.png)

This theme features the Free and Open Source [Inter Font Family](https://rsms.me/inter/)


